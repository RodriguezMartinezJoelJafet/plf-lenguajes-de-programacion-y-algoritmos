# **Tecnológico Nacional de México**
**Instituto Tecnológico de Oaxaca**
## Programación Lógica y Funcional
SCC1019 ISB

### Act 1. Lenguajes de Programación y Algoritmos

#### _**Mapa 1: ¿Que ha cambiado programando ordenadores entre los 80's y ahora?**_
Año de autoria: 2016

```plantuml
@startmindmap

*[#Cyan] Programando ordenadores \nen los 80 y ahora. \n¿Qué ha cambiado?
**[#GreenYellow] Los sistemas disponen de más recursos, \ny ejercen una mayor carga de trabajo, \npero no son distintos a los sistemas antiguos
***[#GreenYellow] Se comparan las dos épocas:
****[#Orange] Antiguo
*****_ Se define como
******[#Tomato] Sistemas que solamente fueron \ndescontinuados o ya no se \ncomercializan
*******[#Gold] Caracteristicas
********[#HotPink] Computadoras con mediciones de potencia bajas (Mhz)
********[#HotPink] Cada computadora era "un mundo"
********[#HotPink] Se programaba bajo lenguaje ensamblador
*******[#Gold] Ventajas
********[#HotPink] La programación era, a opinión de \nlos desarrolladores, más \nexquisita y satisfactoria
********[#HotPink] El uso de Ensamblador hacia que \ncada recurso se aprovechara
********[#HotPink] Los tiempos de ejecución eran (o son) más fluidos
*******[#Gold] Desventajas
********[#HotPink] Los recursos eran bastante limitados
********[#HotPink] Habia una inmensa variedad de computadoras
********[#HotPink] El lenguaje ensamblador es dificil de comprender

****[#Orange] Actual
*****_ Se define como
******[#Tomato] Sistemas que, como su nombre lo dice, \nestán presentes y comercializados \nen la actualidad
*******[#Gold] Caracteristicas
********[#HotPink] Computadoras con mediciones de potencia más altas (Ghz)
********[#HotPink] Las computadoras son ahora más estandarizadas
********[#HotPink] Hay una inmensa variedad de lenguajes de alto nivel para programar
*******[#Gold] Ventajas
********[#HotPink] La programación es más fluida y \nentendible para el desarrollador
********[#HotPink] Ya no es necesario saber las caracteristicas \nde una computadora más a fondo para \npoder programar
********[#HotPink] Se pueden ejecutar más tareas y mucho más complejas
*******[#Gold] Desventajas
********[#HotPink] La Ley de Moore, cada dia se va agotando
*********[#SpringGreen] La Ley de Moore dicta que, cada 18 meses, \nse duplica el número de transistores en un procesador
**********[#SpringGreen] Sin embargo, cada dia, este plazo se va \nalargando más y más, provocando un estancamiento \nactual de potencia de procesamiento
********[#HotPink] Los lenguajes de alto nivel no logran ser tan \nconsistentes como uno de bajo nivel
********[#HotPink] El numero de actualizaciones hace que \nse sienta un programa "incompleto"

@endmindmap
```

#### _**Mapa 2: Historia de los algoritmos y los lenguajes de Programación**_
Año de Autoria: 2010

```plantuml
@startmindmap

*[#Cyan] Historia de algoritmos \nylos lenguajes de \nprogramación
**[#GreenYellow] La historia de estos dos elementos \ninicia desde la misma existencia \ndel ser humano

***[#Orange] Algoritmo
****[#Tomato] Es una serie de pasos detallados, \nprecisos y finitos que conducen \na la realización eficiente \nde alguna labor
*****[#Tomato] Su principal utilidad es para \nsolucionar un problema en especifico
******[#Gold] Ejemplos
*******[#HotPink] Manual de instrucciones
*******[#HotPink] Receta de cocina
*******[#HotPink] Ordenes de trabajo
******[#Gold] Tipos
*******_ Por tamaño
********[#HotPink] Exponencial
********[#HotPink] Polinomiales
*******_ Por lectura
********[#HotPink] Razonable
********[#HotPink] No Razonable
******[#Gold] Caracteristicas
*******[#HotPink] No se puede comprobar su efectividad hasta que se ejecute
*******[#HotPink] Requiere de algún medio para ejecutarse; \nya sea por una computadora, o \nhecho a lápiz y papel
*******[#HotPink] Es perfecto para las computadoras, \nya que estas son muy mecánicas, \ny el algoritmo, muy delimitado
******[#Gold] Elementos
*******[#HotPink] Lectura comprensible
*******[#HotPink] Entradas, procesos y salidas

***[#Orange] Lenguaje de \nProgramación
****[#Tomato] Tiene su origen desde la creación de las \nprimeras "computadoras", desde la \nmáquina de Babbage
*****[#Tomato] Es la representación formal de \nun algoritmo existente, \ninformaticamente hablando
******[#Gold] Evolución
*******[#HotPink] Tablas perforadas, ya sea \nde papel o metal
*******[#HotPink] COBOL, FORTRAN, Lisp, Prolog, C
*******[#HotPink] Java, C++, Phyton
******[#Gold] Tipos
*******[#HotPink] Imperativo
*******[#HotPink] Funcional
*******[#HotPink] Lógica
*******[#HotPink] Orientada a Objetos

@endmindmap
```

#### _**Mapa 3: Las tendencias actuales en los lenguajes de Programación y Sist. Informáticos: Su evolución y paradigmas**_
Año de Autoria: 2005

```plantuml
@startmindmap

*[#Cyan] Tendencias, evolución y paradigmas \nde los Lenguajes de Programación \ny Sist. Informáticos
**[#GreenYellow] Un paradigma es una forma de pensar; \nun ejemplo que se debe de \nacatar en determinada sitación
***[#GreenYellow] Los paradigmas computacionales nacen \ndesde la concepción del \nLenguaje Ensamblador
****[#GreenYellow] Los tipos de paradigma son:
*****[#Orange] Lógico
******[#Tomato] Caracteristicas
*******[#Gold] Se basa en las reglas lógicas, \npremisas en TRUE o FALSE
*******[#Gold] No se asignan variables
*******[#Gold] Prolog y Lisp son ejemplos de leng. lógico
*****[#Orange] Distribuido
******[#Tomato] Caracteristicas
*******[#Gold] Son escalables
*******[#Gold] Se interconectan entre computadoras
*******[#Gold] Ada y Limbo son ejemplos de un Leng. Distribuido
*****[#Orange] Funcional
******[#Tomato] Caracteristicas
*******[#Gold] Se clasifican en puros e hibridos
*******[#Gold] Hace uso de las mátematicas \ny la recursividad
*******[#Gold] F Y Haskell son ejemplos de Leng. Funcional
*****[#Orange] Orientada a Objetos 
******[#Tomato] Caracteristicas
*******[#Gold] Su uso se basa en abstracciones y objetos
*******[#Gold] Tiene herencias, encapsulamiento, polimorfismos, etc.
*******[#Gold] Java y PHP son ejemplos claros de la POO
*****[#Orange] Orientado a Aspectos
******[#Tomato] Caracteristicas
*******[#Gold] Es compartible con varios lenguajes de programación
*******[#Gold] Se basa en capas y módulos
*******[#Gold] Es limpio para programar, pero puede sufrir problemas de diseño
*******[#Gold] Cool es un ejemplo de este paradigma
*****[#Orange] Orientado a Componentes
******[#Tomato] Caracteristicas
*******[#Gold] Se definen bien las interfaces a utilizar
*******[#Gold] Utiliza la comuniccación entre componentes

@endmindmap
```
